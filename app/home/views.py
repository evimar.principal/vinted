# app/home/views.py

from flask import render_template
from flask_login import login_required

from . import home

@home.route('/')
def homepage():
    """
    Render the homepage template on the / route
    """
    title="Sell, swap and buy fashion outfits, secondhand clothing and accessories!"
    return render_template('home/index.html', title = title)

@home.route('/sell-now')
@login_required
def sellnow():
    """
    Render the dashboard template on the /sell-now route
    """
    return render_template('home/sell-now.html', title="Sell now!")