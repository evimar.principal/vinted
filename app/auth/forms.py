# app/auth/forms.py

from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, EqualTo, length

from ..models import User
from re import match

MIN_ZONE, MAX_ZONE = 'T1X', 'T4B'
PATTERN_CANADIAN_POSTAL_ZONE = '[ABCEGHJKLMNPRSTVXY][1-4][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]'
LEN_ZONE = len(MIN_ZONE)
SPACE =' '

class RegistrationForm(FlaskForm):
    """
    Form for users to create new account
    """
    user_login = StringField('Username', validators=[DataRequired(), length(min=3, max=20,message='Username must have min 3 and max 20 characters')],
    					description=u'Pick something you really like. Once chosen you are stuck with it, so choose wisely. Please use letters and numbers only.')
    user_email = StringField('Email', validators=[DataRequired(), Email()])

    user_password = PasswordField('Password', validators=[DataRequired(), 
    														length(min=7, message='Password must be at least 7 chars')],
    					description=u'It should contain at least 7 letters, at least one of them must be a number')
    user_postal = StringField('Postal Code', validators=[DataRequired(), 
    														length(min=7, max=7, message='Use Canadian Postal code format A1A 1A1 with 7 characters, please include one space between ')],
    					description=u'At this moment, this community is for Calgary residents only')
    submit = SubmitField('Sign up')                            
    

    def validate_user_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email is already in use.')

    def validate_user_login(self, field):
        if User.query.filter_by(login=field.data).first():
            raise ValidationError('Username is already in use.')
            
    def validate_user_postal(self, field):
    	  if len(field.data)==LEN_ZONE:
    	  	   field.data += SPACE
    	  if len(field.data)>LEN_ZONE:
    	      zone = field.data[:LEN_ZONE]
    	      if zone < MIN_ZONE or zone > MAX_ZONE:
    	          raise ValidationError('Only Calgary zones are allowed')
    	          
    	      if  match(PATTERN_CANADIAN_POSTAL_ZONE, field.data) is None:
    	          raise ValidationError(field.data+' is not a valid Canadian Postal Code')

    def validate_user_password(self, field):	
        #at least one digit
        digit = False  
        for c in field.data:
    	  	   if c.isdigit():
   	  	       digit = True
   	  	       break
        if not digit:
            raise ValidationError('The password must be at least a number')
    	  	       
    	  	       
    	  	       
    	  	       
    	  	       

class LoginForm(FlaskForm):
    """
    Form for users to login
    """
    user_email = StringField('Email', validators=[DataRequired(), Email()])
    user_password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')
