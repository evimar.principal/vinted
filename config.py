# config.py

class Config(object):
    """
    Common configurations
    """

    # Put any configurations here that are common across all environments
    FIRST_POSTAL_ZONE = 'T1X'
    LAST_POSTAL_ZONE = 'T4B'

class DevelopmentConfig(Config):
    """
    Development configurations
    """

    DEBUG = 1
    SQLALCHEMY_ECHO = True
    TESTING = True

class ProductionConfig(Config):
    """
    Production configurations
    """

    DEBUG = False

app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}
